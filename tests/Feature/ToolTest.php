<?php


class ToolTest extends TestCase
{
    /**
     * @var Jwt Token
     */
    protected $token;

    public function testIndexWithoutTokenParam()
    {
        $response = $this->call('GET', '/tools');
        $this->assertEquals(401, $response->status());
        $this->assertEquals('Token not provided', $response->getOriginalContent()['message']);
    }

    public function testIndexWithExpiredToken()
    {
        $response = $this->call('GET', '/tools?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6MzAwMFwvYXV0aFwvdG9rZW4iLCJpYXQiOjE1NjY1NzcxNjUsImV4cCI6MTU2NjU4MDc2NSwibmJmIjoxNTY2NTc3MTY1LCJqdGkiOiJLbUM5aHdxN3ZxU29aUUlGIiwic3ViIjoxLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.C2EKZWfGB7hooUJe2clj3Tr9q2nBVLoXnvGDx_Uupwg');
        $this->assertEquals(401, $response->status());
        $this->assertEquals('Token has expired', $response->getOriginalContent()['message']);
    }

    public function testTools()
    {
        // Arrange
        $this->generateToken();
        $this->createTool();

        // Act e assert
        $this->json('GET', "tools?token={$this->token}")
            ->seeJsonStructure([
                0 => [
                    'id',
                    'title',
                    'link',
                    'description',
                    'tags',
                ]
            ]);

        // Reset
        $this->deleteTool();
    }

    public function testToolsById()
    {
        // Arrange
        $this->generateToken();
        $this->createTool();

        // Act
        $response = $this->call('GET', "tools/{$this->tool->id}?token={$this->token}");
        $response = json_decode($response->getContent());

        // Assert
        $this->assertEquals($this->tool, $response);

        // Reset
        $this->deleteTool();
    }

    public function testCreateNewTool()
    {
        // Arrange
        $this->generateToken();

        $data = [
            'title' => 'New Tool',
            'link' => 'https://newtooltest.com',
            'description' => 'New Description.',
            'tags' => [
                'test',
                'vuttr',
                'phpunit',
                'new'
            ]
        ];

        // Act
        $response = $this->call('POST', "tools?token={$this->token}", $data);

        $tool = json_decode($response->getContent());

        // Assert
        $this->assertEquals(201, $response->getStatusCode());
        $this->assertEquals($data['title'], $tool->title);
        $this->assertEquals($data['link'], $tool->link);
        $this->assertEquals($data['description'], $tool->description);
        $this->assertEquals($data['tags'], $tool->tags);

        // Reset
        $this->deleteTool($tool->id);
    }

    public function testUpdateTool()
    {
        // Arrange
        $this->generateToken();
        $this->createTool();

        $updatedData = [
            'title' => 'Updated Tool',
            'link' => 'https://updatedtooltest.com',
            'description' => 'Updated Description.',
            'tags' => [
                'phpunit',
                'updated'
            ]
        ];

        // Act
        $response = $this->call('PUT', "tools/{$this->tool->id}?token={$this->token}", $updatedData);

        $tool = json_decode($response->getContent());

        // Assert
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals($updatedData['title'], $tool->title);
        $this->assertEquals($updatedData['link'], $tool->link);
        $this->assertEquals($updatedData['description'], $tool->description);
        $this->assertEquals($updatedData['tags'], $tool->tags);

        // Reset
        $this->deleteTool($tool->id);
    }

    public function testDeleteTool()
    {
        // Arrange
        $this->generateToken();
        $this->createTool();

        // Act
        $deleteMethodResponse = $this->call('DELETE', "tools/{$this->tool->id}?token={$this->token}");

        // Asserts

        # Try to retrieve a deleted tool
        $getMethodResponse = $this->call('GET', "tools/{$this->tool->id}?token={$this->token}");

        # Expected get method response
        $expectedGetMethodResponse = new \stdClass();
        $expectedGetMethodResponse->message = "No query results for model [App\Tool] {$this->tool->id}";
        $expectedGetMethodResponse->status_code = 404;

        $this->assertEquals(204, $deleteMethodResponse->getStatusCode());
        $this->assertEquals(404, $getMethodResponse->getStatusCode());
        $this->assertEquals($expectedGetMethodResponse, json_decode($getMethodResponse->getContent()));
    }

    /**
     * @return mixed
     */
    private function generateToken()
    {
        $this->createTestUser();

        // Testing User credentials (use for test)
        $credentials = [
            'email' => 'vuttrtest@vuttr.com',
            'password' => 'secret'
        ];

        $response = $this->call('POST', '/auth/token', $credentials);
        $this->token = $response->getOriginalContent()['token'];
    }

    /**
     * Create a new tool
     */
    private function createTool()
    {
        $data = [
            'title' => 'Tool Test',
            'link' => 'https://tooltest.com',
            'description' => 'Description test.',
            'tags' => [
                'test',
                'vuttr',
                'phpunit',
            ]
        ];
        $response = $this->call('POST', "tools?token={$this->token}", $data);
        $this->tool = json_decode($response->getContent());
    }

    /**
     * Delete a tool
     * @param int $toolId
     * @return \Illuminate\Http\Response
     */
    private function deleteTool(int $toolId = null)
    {
        $toolId = $toolId ?? $this->tool->id;
        $response = $this->call('DELETE', "tools/{$toolId}?token={$this->token}");
    }

    /**
     * Create a new user
     */
    private function createTestUser()
    {
        $userData = [
            'name' => 'Vuttr User Test',
            'email' => 'vuttrtest@vuttr.com',
            'password' => 'secret',
        ];

        $response = $this->call('POST', 'users', $userData);
        if ($response->getStatusCode() == 201) {
            $this->user = json_decode($response->getOriginalContent());
        }
    }
}
