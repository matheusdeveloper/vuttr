<?php

use App\Http\Controllers\UserController;
use App\Interfaces\Repositories\UserRepositoryInterface;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Hash;

class UserControllerTest extends TestCase
{
    /**
     * @var \Mockery\Mock
     */
    protected $userRepositoryMock;

    /**
     * @var \Mockery\Mock
     */
    protected $userControllerMock;

    public function setUp(): void
    {
        parent::setUp();

        $this->userRepositoryMock = Mockery::mock(UserRepositoryInterface::class)->makePartial();
        $this->userControllerMock = Mockery::mock(UserController::class, [$this->userRepositoryMock])->makePartial();
        $this->app->instance(UserRepositoryInterface::class, $this->userRepositoryMock);
    }

    public function testIndex()
    {
        // Arrange
        $users = factory(User::class, 3)->make();

        $this->userRepositoryMock
            ->shouldReceive('all')
            ->andReturn($users);

        // Act
        $apiResponse = $this->get('users');
        $expectedJson = json_encode(['data' => $users->jsonSerialize()]);

        // Assert
        $this->assertJsonStringEqualsJsonString($expectedJson, $apiResponse->response->getContent());
        $this->assertEquals(200, $apiResponse->response->getStatusCode());
    }

    public function testIndexWithSearchingByEmail()
    {
        // Arrange
        $users = factory(User::class, 2)->make();

        $this->userRepositoryMock
            ->shouldReceive('findWhere')
            ->andReturn($users);

        // Act
        $apiResponse = $this->get('users?email=user@email.com');
        $expectedJson = json_encode(['data' => $users->jsonSerialize()]);

        // Assert
        $this->assertJsonStringEqualsJsonString($expectedJson, $apiResponse->response->getContent());
        $this->assertEquals(200, $apiResponse->response->getStatusCode());
    }

    public function testIndexWithEmptyResults()
    {
        // Arrange
        $this->userRepositoryMock
            ->shouldReceive('all')
            ->andReturn(collect([]));

        // Act
        $apiResponse = $this->get('users');
        $expectedJson = json_encode(['message' => 'Not Found', 'status_code' => 404], true);

        // Assert
        $this->assertJsonStringEqualsJsonString($expectedJson, $apiResponse->response->getContent());
        $this->assertEquals(404, $apiResponse->response->getStatusCode());
    }

    public function testShow()
    {
        // Arrange
        $user = factory(User::class)->make();
        $this->userRepositoryMock->shouldReceive('find')->andReturn($user);

        $expectedJson = json_encode($user->jsonSerialize());

        // Act
        $apiResponse = $this->get('users/1');

        // Assert
        $this->assertJsonStringEqualsJsonString($expectedJson, $apiResponse->response->getContent());
        $this->assertEquals(200, $apiResponse->response->getStatusCode());
    }

    public function testShowWithInvalidId()
    {
        // Arrange
        $userId = 30;
        $exception = (new ModelNotFoundException)->setModel(User::class, $userId);

        $this->userRepositoryMock->shouldReceive('find')->andThrow($exception);

        $expectedJson = json_encode([
            'message' => 'No query results for model [App\\User] 30',
            'status_code' => 404
        ]);

        // Act
        $apiResponse = $this->get("users/{$userId}");

        // Assert
        $this->assertJsonStringEqualsJsonString($expectedJson, $apiResponse->response->getContent());
        $this->assertEquals(404, $apiResponse->response->getStatusCode());
    }

    public function testStore()
    {
        // Arrange
        $user = factory(User::class)->make();

        $userArray = array_merge($user->toArray(), ['password' => Hash::make('secret')]);
        $this->userRepositoryMock->shouldReceive('create')->andReturn($user);
        $this->userRepositoryMock->shouldReceive('exists')->andReturn(false);

        $expectedJson = json_encode($user->jsonSerialize());

        // Act
        $apiResponse = $this->post('users', $userArray);

        // Assert
        $this->assertJsonStringEqualsJsonString($expectedJson, $apiResponse->response->getContent());
        $this->assertEquals(201, $apiResponse->response->getStatusCode());
    }

    /**
     * @dataProvider storeUsersWithoutParamsdataProvider
     */
    public function testStoreWithInvalidParams($postData, $expectedJson, $validatePassword = false, $alreadyExists = false)
    {
        // Arrange
        $user = new User($postData);

        $userArray = $user->toArray();
        if ($validatePassword) {
            $userArray = array_merge($user->toArray(), ['password' => Hash::make('secret')]);
        }
        $this->userRepositoryMock->shouldReceive('create')->andReturn($user);
        $this->userRepositoryMock->shouldReceive('exists')->andReturn($alreadyExists);

        // Act
        $apiResponse = $this->post('users', $userArray);

        // Assert
        $this->assertJsonStringEqualsJsonString(
            json_encode($expectedJson), $apiResponse->response->getContent()
        );
        $this->assertEquals(422, $apiResponse->response->getStatusCode());
    }

    public function testUpdate()
    {
        // Arrange
        $user = factory(User::class)->make();

        $userArray = array_merge($user->toArray(), ['password' => Hash::make('secret')]);
        $this->userRepositoryMock->shouldReceive('update')->andReturn($user);
        $this->userRepositoryMock->shouldReceive('exists')->andReturn(false);

        $expectedJson = json_encode($user->jsonSerialize());

        // Act
        $apiResponse = $this->put('users/4', $userArray);

        // Assert
        $this->assertJsonStringEqualsJsonString($expectedJson, $apiResponse->response->getContent());
        $this->assertEquals(200, $apiResponse->response->getStatusCode());
    }

    /**
     * @dataProvider storeUsersWithoutParamsdataProvider
     */
    public function testUpdateWithInvalidParams($putData, $expectedJson, $validatePassword = false, $alreadyExists = false)
    {
        // Arrange
        $user = new User($putData);

        $userArray = $user->toArray();
        if ($validatePassword) {
            $userArray = array_merge($user->toArray(), ['password' => Hash::make('secret')]);
        }

        $this->userRepositoryMock->shouldReceive('update')->andReturn($user);
        $this->userRepositoryMock->shouldReceive('exists')->andReturn($alreadyExists);

        // Act
        $apiResponse = $this->put('users/5', $userArray);

        // Assert
        $this->assertJsonStringEqualsJsonString(
            json_encode($expectedJson), $apiResponse->response->getContent()
        );
        $this->assertEquals(422, $apiResponse->response->getStatusCode());
    }

    public function testDelete()
    {
        // Arrange
        $this->userRepositoryMock->shouldReceive('destroy')->andReturn(true);

        // Act
        $apiResponse = $this->delete('users/4');

        // Assert
        $this->assertEquals(204, $apiResponse->response->getStatusCode());
        $this->assertEmpty($apiResponse->response->getContent());
    }

    public function testDeleteWithInvalidUserId()
    {
        // Arrange
        $userId = 40;
        $exception = (new ModelNotFoundException)->setModel(User::class, $userId);

        $this->userRepositoryMock->shouldReceive('destroy')->andThrow($exception);

        $expectedJson = json_encode([
            'message' => 'No query results for model [App\\User] 40',
            'status_code' => 404
        ]);

        // Act
        $apiResponse = $this->delete("users/{$userId}");

        // Assert
        $this->assertJsonStringEqualsJsonString($expectedJson, $apiResponse->response->getContent());
        $this->assertEquals(404, $apiResponse->response->getStatusCode());
    }


    public function storeUsersWithoutParamsdataProvider()
    {
        return [
            [
                ['name' => 'User name'],
                [
                    'message' => 'Could not create new user.',
                    'errors' => [
                        'password' => [
                            'The password field is required.'
                        ],
                        'email' => [
                            'The email field is required.'
                        ],
                    ],
                    'status_code' => 422
                ],
            ],
            [
                ['email' => 'username@vuttr.com'],
                [
                    'message' => 'Could not create new user.',
                    'errors' => [
                        'name' => [
                            'The name field is required.'
                        ],
                        'password' => [
                            'The password field is required.'
                        ],
                    ],
                    'status_code' => 422
                ]
            ],
            [
                ['email' => 'issonaoeumemail'],
                [
                    'message' => 'Could not create new user.',
                    'errors' => [
                        'name' => [
                            'The name field is required.'
                        ],
                        'email' => [
                            'The email must be a valid email address.'
                        ],
                        'password' => [
                            'The password field is required.'
                        ],
                    ],
                    'status_code' => 422
                ]
            ],
            [
                ['email' => 'username@vuttr.com'],
                [
                    'message' => 'Could not create new user.',
                    'errors' => [
                        'name' => [
                            'The name field is required.'
                        ],
                        'password' => [
                            'The password field is required.'
                        ],
                        'email' => [
                            'The email has already been taken.'
                        ],
                    ],
                    'status_code' => 422
                ],
                false,
                true
            ],
            [
                ['password' => 'secret'],
                [
                    'message' => 'Could not create new user.',
                    'errors' => [
                        'name' => [
                            'The name field is required.'
                        ],
                        'email' => [
                            'The email field is required.'
                        ],
                    ],
                    'status_code' => 422
                ],
                true
            ],
        ];
    }
}
