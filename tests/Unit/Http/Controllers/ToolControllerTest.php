<?php

use App\Http\Controllers\ToolController;
use App\Interfaces\Repositories\ToolRepositoryInterface;
use App\Tool;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ToolControllerTest extends TestCase
{
    /**
     * @var \Mockery\Mock
     */
    protected $toolRepositoryMock;

    /**
     * @var \Mockery\Mock
     */
    protected $toolControllerMock;

    public function setUp(): void
    {
        parent::setUp();

        $this->toolRepositoryMock = Mockery::mock(ToolRepositoryInterface::class)->makePartial();
        $this->toolControllerMock = Mockery::mock(ToolController::class, [$this->toolRepositoryMock])->makePartial();
        $this->app->instance(ToolRepositoryInterface::class, $this->toolRepositoryMock);
    }

    public function testIndex()
    {
        // Arrange
        $tools = factory(Tool::class, 3)->make();

        $this->toolRepositoryMock
            ->shouldReceive('all')
            ->andReturn($tools);

        // Act
        $apiResponse = $this->get('tools');
        $expectedJson = json_encode(['data' => $tools->jsonSerialize()]);

        // Assert
        $this->assertJsonStringEqualsJsonString($expectedJson, $apiResponse->response->getContent());
        $this->assertEquals(200, $apiResponse->response->getStatusCode());
    }

    public function testIndexWithSearchingByTag()
    {
        // Arrange
        $tools = factory(Tool::class, 2)->make();

        $this->toolRepositoryMock
            ->shouldReceive('findByTag')
            ->andReturn($tools);

        // Act
        $apiResponse = $this->get('tools?tag=tagname');
        $expectedJson = json_encode(['data' => $tools->jsonSerialize()]);

        // Assert
        $this->assertJsonStringEqualsJsonString($expectedJson, $apiResponse->response->getContent());
        $this->assertEquals(200, $apiResponse->response->getStatusCode());
    }

    public function testIndexWithEmptyResults()
    {
        // Arrange
        $this->toolRepositoryMock
            ->shouldReceive('all')
            ->andReturn(collect([]));

        // Act
        $apiResponse = $this->get('tools');
        $expectedJson = json_encode(['message' => 'Not Found', 'status_code' => 404], true);

        // Assert
        $this->assertJsonStringEqualsJsonString($expectedJson, $apiResponse->response->getContent());
        $this->assertEquals(404, $apiResponse->response->getStatusCode());
    }

    public function testShow()
    {
        // Arrange
        $tool = factory(Tool::class)->make();
        $this->toolRepositoryMock->shouldReceive('find')->andReturn($tool);

        $expectedJson = json_encode($tool->jsonSerialize());

        // Act
        $apiResponse = $this->get('tools/1');

        // Assert
        $this->assertJsonStringEqualsJsonString($expectedJson, $apiResponse->response->getContent());
        $this->assertEquals(200, $apiResponse->response->getStatusCode());
    }

    public function testShowWithInvalidId()
    {
        // Arrange
        $toolId = 30;
        $exception = (new ModelNotFoundException)->setModel(Tool::class, $toolId);

        $this->toolRepositoryMock->shouldReceive('find')->andThrow($exception);

        $expectedJson = json_encode([
            'message' => 'No query results for model [App\\Tool] 30',
            'status_code' => 404
        ]);

        // Act
        $apiResponse = $this->get("tools/{$toolId}");

        // Assert
        $this->assertJsonStringEqualsJsonString($expectedJson, $apiResponse->response->getContent());
        $this->assertEquals(404, $apiResponse->response->getStatusCode());
    }

    public function testStore()
    {
        // Arrange
        $tool = factory(Tool::class)->make();
        $this->toolRepositoryMock->shouldReceive('create')->andReturn($tool);

        $expectedJson = json_encode($tool->jsonSerialize());

        // Act
        $apiResponse = $this->post('tools', $tool->toArray());

        // Assert
        $this->assertJsonStringEqualsJsonString($expectedJson, $apiResponse->response->getContent());
        $this->assertEquals(201, $apiResponse->response->getStatusCode());
    }

    /**
     * @dataProvider storeToolsWithoutParamsdataProvider
     */
    public function testStoreWithInvalidParams($postData, $expectedJson)
    {
        // Arrange
        $tool = new Tool($postData);

        $this->toolRepositoryMock->shouldReceive('create')->andReturn($tool);

        // Act
        $apiResponse = $this->post('tools', $tool->toArray());

        // Assert
        $this->assertJsonStringEqualsJsonString(
            json_encode($expectedJson), $apiResponse->response->getContent()
        );
        $this->assertEquals(422, $apiResponse->response->getStatusCode());
    }

    public function testUpdate()
    {
        // Arrange
        $tool = factory(Tool::class)->make();
        $this->toolRepositoryMock->shouldReceive('update')->andReturn($tool);

        $expectedJson = json_encode($tool->jsonSerialize());

        // Act
        $apiResponse = $this->put('tools/4', $tool->toArray());

        // Assert
        $this->assertJsonStringEqualsJsonString($expectedJson, $apiResponse->response->getContent());
        $this->assertEquals(200, $apiResponse->response->getStatusCode());
    }

    /**
     * @dataProvider storeToolsWithoutParamsdataProvider
     */
    public function testUpdateWithInvalidParams($putData, $expectedJson)
    {
        // Arrange
        $tool = new Tool($putData);

        $this->toolRepositoryMock->shouldReceive('update')->andReturn($tool);

        // Act
        $apiResponse = $this->put('tools/5', $tool->toArray());

        // Assert
        $this->assertJsonStringEqualsJsonString(
            json_encode($expectedJson), $apiResponse->response->getContent()
        );
        $this->assertEquals(422, $apiResponse->response->getStatusCode());
    }

    public function testDelete()
    {
        // Arrange
        $this->toolRepositoryMock->shouldReceive('destroy')->andReturn(true);

        // Act
        $apiResponse = $this->delete('tools/4');

        // Assert
        $this->assertEquals(204, $apiResponse->response->getStatusCode());
        $this->assertEmpty($apiResponse->response->getContent());
    }

    public function testDeleteWithInvalidToolId()
    {
        // Arrange
        $toolId = 40;
        $exception = (new ModelNotFoundException)->setModel(Tool::class, $toolId);

        $this->toolRepositoryMock->shouldReceive('destroy')->andThrow($exception);

        $expectedJson = json_encode([
            'message' => 'No query results for model [App\\Tool] 40',
            'status_code' => 404
        ]);

        // Act
        $apiResponse = $this->delete("tools/{$toolId}");

        // Assert
        $this->assertJsonStringEqualsJsonString($expectedJson, $apiResponse->response->getContent());
        $this->assertEquals(404, $apiResponse->response->getStatusCode());
    }

    public function storeToolsWithoutParamsdataProvider()
    {
        return [
            [
                ['title' => 'Title Test'],
                [
                    'message' => 'Could not create new tool.',
                    'errors' => [
                        'link' => [
                            'The link field is required.'
                        ],
                        'description' => [
                            'The description field is required.'
                        ],
                        'tags' => [
                            'The tags field is required.'
                        ],
                    ],
                    'status_code' => 422
                ]
            ],
            [
                ['link' => 'https://linktest.com.br'],
                [
                    'message' => 'Could not create new tool.',
                    'errors' => [
                        'title' => [
                            'The title field is required.'
                        ],
                        'description' => [
                            'The description field is required.'
                        ],
                        'tags' => [
                            'The tags field is required.'
                        ],
                    ],
                    'status_code' => 422
                ]
            ],
            [
                ['link' => 'urlerrada'],
                [
                    'message' => 'Could not create new tool.',
                    'errors' => [
                        'title' => [
                            'The title field is required.'
                        ],
                        'link' => [
                            'The link format is invalid.'
                        ],
                        'description' => [
                            'The description field is required.'
                        ],
                        'tags' => [
                            'The tags field is required.'
                        ],
                    ],
                    'status_code' => 422
                ]
            ],
            [
                ['description' => 'description test'],
                [
                    'message' => 'Could not create new tool.',
                    'errors' => [
                        'title' => [
                            'The title field is required.'
                        ],
                        'link' => [
                            'The link field is required.'
                        ],
                        'tags' => [
                            'The tags field is required.'
                        ],
                    ],
                    'status_code' => 422
                ]
            ],
            [
                ['tags' => ['node', 'php']],
                [
                    'message' => 'Could not create new tool.',
                    'errors' => [
                        'title' => [
                            'The title field is required.'
                        ],
                        'link' => [
                            'The link field is required.'
                        ],
                        'description' => [
                            'The description field is required.'
                        ],
                    ],
                    'status_code' => 422
                ]
            ],
            [
                ['tags' => 'não é um array'],
                [
                    'message' => 'Could not create new tool.',
                    'errors' => [
                        'title' => [
                            'The title field is required.'
                        ],
                        'link' => [
                            'The link field is required.'
                        ],
                        'description' => [
                            'The description field is required.'
                        ],
                        'tags' => [
                            'The tags must be an array.'
                        ],
                    ],
                    'status_code' => 422
                ]
            ],
        ];
    }
}
