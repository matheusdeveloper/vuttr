# VUTTR

Vuttr (Very Useful Tools to Remember) BossaBox Challenge 

https://vuttrchallenge.herokuapp.com

**Open API** (swagger) https://vuttrchallenge.herokuapp.com/api-docs

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites


* Git
* Docker
* Docker-compose


### Installing

All vuttr installation is made throught Docker.


Clone the repository

```
git clone git@gitlab.com:matheusdeveloper/vuttr.git
```

Build image and up containers

```
docker-compose build
docker-compose up -d
```

After that, 4 containers will be mouted:

* vuttr_nginx
* vuttr_phpmyadmin
* vuttr_mysql
* vuttr_web

To access the container, execute: 

```
docker-compose exec web bash
```


After all, inside container **web**, run commands to install dependencies, migrate database and populate users table:

```
composer install
php artisan migrate
php artisan db:seed
```


## Running the tests

Inside **web** container, run tests:


```
vendor/bin/phpunit
```


## Deployment

The vuttr deployment uses [GitLab CI](https://docs.gitlab.com/ee/ci/) and [Heroku: Cloud Application Platform](https://www.heroku.com).


Every time code is merged into the **master** or **develop** branch, GitLab CI deploys to the **[staging](https://vuttr-staging.herokuapp.com)** or **[production](https://vuttrchallenge.herokuapp.com)** environment respectively.


## Built With

* [Lumen](https://lumen.laravel.com/) - The web framework used


## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
