<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/api-docs', function ()  {
    return view('api_docs');
});

$api = app('Dingo\Api\Routing\Router');

$middleware = env('APP_ENV') == 'testing' ? [] : ['middleware' => 'jwt.auth'];

$api->version('v1', function ($api) use ($middleware) {
    // auth
    $api->post('auth/token', 'App\Http\Controllers\AuthController@authenticate');
    $api->post('users', ['middleware' => 'users-validation', 'uses' => 'App\Http\Controllers\UserController@store']);

    // private
    $api->group($middleware, function ($api) {
        $api->get('tools', 'App\Http\Controllers\ToolController@index');
        $api->get('tools/{id}', 'App\Http\Controllers\ToolController@show');
        $api->post('tools', ['middleware' => 'tools-validation', 'uses' => 'App\Http\Controllers\ToolController@store']);
        $api->put('tools/{id}', ['middleware' => 'tools-validation', 'uses' => 'App\Http\Controllers\ToolController@update']);
        $api->delete('tools/{id}', 'App\Http\Controllers\ToolController@destroy');

        $api->get('users/{id}', 'App\Http\Controllers\UserController@show');
        $api->get('users', 'App\Http\Controllers\UserController@index');
        $api->put('users/{id}', ['middleware' => 'users-validation', 'uses' => 'App\Http\Controllers\UserController@update']);
        $api->delete('users/{id}', 'App\Http\Controllers\UserController@destroy');
    });
});
