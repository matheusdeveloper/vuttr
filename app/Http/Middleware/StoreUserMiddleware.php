<?php

namespace App\Http\Middleware;

use App\Interfaces\Repositories\UserRepositoryInterface;
use Closure;
use Dingo\Api\Exception\StoreResourceFailedException;

class StoreUserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'email|required',
            'password' => 'required',
        ]);

        $validator->after(function ($validator) use ($request) {
            if (app(UserRepositoryInterface::class)->exists('email', $request->get('email'))) {
                $validator->errors()->add('email', 'The email has already been taken.');
            }
        });

        if ($validator->fails()) {
            throw new StoreResourceFailedException('Could not create new user.', $validator->errors());
        }

        return $next($request);
    }
}
