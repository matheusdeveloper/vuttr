<?php

namespace App\Http\Middleware;

use Closure;
use Dingo\Api\Exception\StoreResourceFailedException;

class StoreToolMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       $validator = \Validator::make($request->all(), [
           'title' => 'required',
           'link' => 'required|url',
           'description' => 'required',
           'tags' => 'required|array',
       ]);

       if ($validator->fails()) {
           throw new StoreResourceFailedException('Could not create new tool.', $validator->errors());
       }

        return $next($request);
    }
}
