<?php

namespace App\Http\Controllers;

use App\Interfaces\Repositories\UserRepositoryInterface;
use App\Transformers\UserTransformer;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * ToolController constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param Request $request
     * @return \Dingo\Api\Http\Response
     */
    public function index(Request $request)
    {
        if ($filteredEmail = $request->get('email')) {
            $users = $this->userRepository->findWhere(['email' => $filteredEmail]);
        } else {
            $users = $this->userRepository->all();
        }

        if ($users->isEmpty()) {
            return $this->response->errorNotFound();
        }
        return $this->response->collection($users, new UserTransformer());
    }

    /**
     * @param int $id
     * @return \Dingo\Api\Http\Response|void
     */
    public function show(int $id)
    {
        $tool = $this->userRepository->find($id);
        return $this->responseWithSerializer($tool, new UserTransformer());
    }

    /**
     * @param Request $request
     * @return \Dingo\Api\Http\Response
     */
    public function store(Request $request)
    {
        $tool = $this->userRepository->create($request->all());
        return $this->responseWithSerializer($tool, new UserTransformer(), 201);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Dingo\Api\Http\Response
     */
    public function update(Request $request, int $id)
    {
        $tool = $this->userRepository->update($id, $request->all());
        return $this->responseWithSerializer($tool, new UserTransformer(), 200);
    }

    /**
     * @param int $id
     * @return \Dingo\Api\Http\Response
     */
    public function destroy(int $id)
    {
        $this->userRepository->destroy($id);
        return $this->response->noContent();
    }
}
