<?php

namespace App\Http\Controllers;

use App\Interfaces\Repositories\ToolRepositoryInterface;
use App\Repositories\ToolRepository;
use App\Transformers\ToolTransformer;
use Illuminate\Http\Request;

class ToolController extends Controller
{
    /**
     * @var ToolRepositoryInterface
     */
    private $toolRepository;

    /**
     * ToolController constructor.
     * @param ToolRepository $toolRepository
     */
    public function __construct(ToolRepositoryInterface $toolRepository)
    {
        $this->toolRepository = $toolRepository;
    }

    /**
     * @param Request $request
     * @return \Dingo\Api\Http\Response|void
     */
    public function index(Request $request)
    {
        if ($filteredTag = $request->get('tag')) {
            $tools = $this->toolRepository->findByTag($filteredTag);
        } else {
            $tools = $this->toolRepository->all();
        }

        if ($tools->isEmpty()) {
            return $this->response->errorNotFound();
        }
        return $this->response->collection($tools, new ToolTransformer());
    }

    /**
     * @param int $id
     * @return \Dingo\Api\Http\Response|void
     */
    public function show(int $id)
    {
        $tool = $this->toolRepository->find($id);
        return $this->responseWithSerializer($tool, new ToolTransformer());
    }

    /**
     * @param Request $request
     * @return \Dingo\Api\Http\Response
     */
    public function store(Request $request)
    {
        $tool = $this->toolRepository->create($request->all());
        return $this->responseWithSerializer($tool, new ToolTransformer(), 201);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Dingo\Api\Http\Response
     */
    public function update(Request $request, int $id)
    {
        $tool = $this->toolRepository->update($id, $request->all());
        return $this->responseWithSerializer($tool, new ToolTransformer(), 200);
    }

    /**
     * @param int $id
     * @return \Dingo\Api\Http\Response
     */
    public function destroy(int $id)
    {
        $this->toolRepository->destroy($id);
        return $this->response->noContent();
    }
}
