<?php

namespace App\Http\Controllers;

use Dingo\Api\Routing\Helpers;
use Laravel\Lumen\Routing\Controller as BaseController;
use League\Fractal\Serializer\ArraySerializer;

class Controller extends BaseController
{
    use Helpers;

    /**
     * @param $data
     * @param $transformer
     * @param int $statusCode
     * @return \Dingo\Api\Http\Response
     */
    public function responseWithSerializer($data, $transformer, int $statusCode = 200)
    {
        return $this->response->item($data, $transformer, function ($resource, $fractal) {
            $fractal->setSerializer(new ArraySerializer());
        })->setStatusCode($statusCode);
    }
}
