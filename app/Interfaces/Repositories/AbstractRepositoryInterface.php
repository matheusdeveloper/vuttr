<?php


namespace App\Interfaces\Repositories;


interface AbstractRepositoryInterface
{
    public function model(): string ;
}