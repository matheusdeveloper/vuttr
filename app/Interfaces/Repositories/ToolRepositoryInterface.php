<?php


namespace App\Interfaces\Repositories;


interface ToolRepositoryInterface
{
    public function model(): string;
}
