<?php


namespace App\Interfaces\Repositories;


interface UserRepositoryInterface
{
    public function model(): string;
}
