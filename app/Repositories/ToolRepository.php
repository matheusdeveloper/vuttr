<?php

namespace App\Repositories;

use App\Interfaces\Repositories\ToolRepositoryInterface;
use App\Tool;


class ToolRepository extends AbstractRepository implements ToolRepositoryInterface
{
    /**
     * @return string
     */
    public function model(): string
    {
        return Tool::class;
    }

    /**
     * @param string $tag
     * @return mixed
     */
    public function findByTag(string $tag)
    {
        return $this->model
            ->whereJsonContains('tags', $tag)
            ->get();
    }
}
