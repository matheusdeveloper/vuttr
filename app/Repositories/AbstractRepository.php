<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

abstract class AbstractRepository
{
    /**
     * Eloquent model
     * @var Model
     */
    protected $model;

    /**
     * AbstractRepository constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->makeModel();
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function create(array $params)
    {
        return $this->model->create($params);
    }

    /**
     * @param $field
     * @param $value
     * @return bool
     */
    public function exists($field, $value): bool
    {
        return $this->model->where($field, $value)->exists();
    }

    /**
     * @param int $id
     * @param array $params
     * @return mixed
     */
    public function update(int $id, array $params)
    {
        $model = $this->model->findOrFail($id);
        $model->fill($params);
        $model->save();

        return $model;
    }

    /**
     * @param array $columns
     * @return \Illuminate\Database\Eloquent\Collection|Model[]
     */
    public function all(array $columns = ['*'])
    {
        return $this->model->all($columns);
    }

    /**
     * @param int $limit
     * @param array $columns
     * @return mixed
     */
    public function paginate(int $limit, $columns = ['*'])
    {
        return $this->model->paginate($limit, $columns);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function find(int $id)
    {
        return $this->model->findOrFail($id, ['*']);
    }

    /**
     * @param int $id
     * @param array $columns
     * @param string $message
     * @return mixed
     */
    public function findOrFail(int $id, array $columns = ['*'], string $message = '')
    {
        try {
            return $this->model->findOrFail($id, $columns);
        } catch (ModelNotFoundException $t) {
            throw new ModelNotFoundException($message);
        }
    }

    /**
     * @param array $where
     * @param array $columns
     * @return mixed
     */
    public function findWhere(array $where, $columns = ['*'])
    {
        $this->applyConditions($where);
        return $this->model->get($columns);
    }

    /**
     * @param $field
     * @param array $values
     * @param array $columns
     * @return mixed
     */
    public function findWhereIn($field, array $values, $columns = ['*'])
    {
        return $this->model->whereIn($field, $values)->get($columns);
    }

    /**
     * @param $id
     * @return int
     */
    public function destroy($id)
    {
        $this->find($id);
        return $this->model->destroy($id);
    }

    /**
     * @return Model|mixed
     * @throws \Exception
     */
    protected function makeModel()
    {
        $model = app()->make($this->model());
        if (!$model instanceof Model) {
            throw new \Exception("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");
        }
        return $this->model = $model;
    }

    /**
     * Applies the given where conditions to the model.
     * @param array $where
     */
    protected function applyConditions(array $where)
    {
        foreach ($where as $field => $value) {
            if (is_array($value)) {
                list($field, $condition, $val) = $value;
                $this->model = $this->model->where($field, $condition, $val);
            } else {
                $this->model = $this->model->where($field, '=', $value);
            }
        }
    }
}
