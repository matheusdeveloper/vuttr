<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;

class ToolTransformer extends TransformerAbstract
{
    public function transform($tool)
    {
        return [
            'id' => (int)$tool->id,
            'title' => $tool->title,
            'link' => $tool->link,
            'description' => $tool->description,
            'tags' => $tool->tags,
        ];
    }
}
