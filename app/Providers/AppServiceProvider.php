<?php

namespace App\Providers;

use App\Interfaces\Repositories\ToolRepositoryInterface;
use App\Interfaces\Repositories\UserRepositoryInterface;
use App\Repositories\ToolRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ToolRepositoryInterface::class, ToolRepository::class);
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
    }
}
