FROM php:7.2-fpm-alpine

# Set default work directory
WORKDIR /var/www

# Start as root
USER root

# Install the PHP pdo_mysql extention
RUN docker-php-ext-install pdo_mysql

#
#--------------------------------------------------------------------------
# Software's Installation
#--------------------------------------------------------------------------
#
# Install "libraries", "Software's"

RUN apk update && \
    apk add --no-cache util-linux \
    bash \
    git \
    curl \
    vim \
    nano

#
#--------------------------------------------------------------------------
# docker non-root user:
#--------------------------------------------------------------------------
#

# Add a non-root user to prevent files being created with root permissions on host machine.
ARG PUID=1000
ENV PUID 1000
ARG PGID=1000
ENV PGID 1000

RUN addgroup -g ${PGID} docker && \
    adduser -D -u ${PUID} -G docker docker



# Install composer and add its bin to the PATH.
RUN curl -s http://getcomposer.org/installer | php && \
    echo "export PATH=${PATH}:/var/www/vendor/bin" >> ~/.bashrc && \
    mv composer.phar /usr/local/bin/composer && \
    composer global require "hirak/prestissimo:^0.3"

# Source the bash
RUN . ~/.bashrc

#
#--------------------------------------------------------------------------
# Final Touch
#--------------------------------------------------------------------------
#

# Clean up
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
    rm /var/log/lastlog /var/log/faillog \
    rm -rf /var/cache/apk/*

CMD ["php-fpm"]

EXPOSE 9000

