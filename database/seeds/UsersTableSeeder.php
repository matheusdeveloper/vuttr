<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();

        DB::table('users')->insert([
            'name' => 'User Test',
            'email' => 'vuttrtest@vuttr.com',
            'password' => Hash::make('vuttrtest'),
            'created_at' => Carbon::now(),
        ]);

        DB::table('users')->insert([
            'name' => 'Matheus Lima',
            'email' => 'ma.limarodrigues@gmail.com',
            'password' => Hash::make('secret'),
            'created_at' => Carbon::now(),
        ]);
    }
}
