<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'id' => $faker->randomDigitNotNull,
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => $faker->password
    ];
});

$factory->define(App\Tool::class, function (Faker\Generator $faker) {
    return [
        'id' => $faker->randomDigitNotNull,
        'title' => $faker->sentence(3),
        'link' => $faker->url,
        'description' => $faker->text,
        'tags' => [
            $faker->word,
            $faker->word,
            $faker->word,
        ],
        'created_at' => \Carbon\Carbon::now(),
        'updated_at' => \Carbon\Carbon::now(),
    ];
});
