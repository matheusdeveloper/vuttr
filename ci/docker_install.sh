#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# Install tools (the php image doesn't have it)
apk update
apk add --no-cache util-linux bash git curl ruby-dev ruby-dev ruby-rdoc

# Install composer and add its bin to the PATH.
curl -s http://getcomposer.org/installer | php && \
echo "export PATH=${PATH}:/var/www/vendor/bin" >> ~/.bashrc && \
mv composer.phar /usr/local/bin/composer && \
composer global require "hirak/prestissimo:^0.3"